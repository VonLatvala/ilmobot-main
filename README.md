# IlmoBot

![TiK IlmoBot Logo](https://bitbucket.org/axellatvala/ilmobot-main/raw/d36bc1c5a31dca90e63e5fbfa70d83098263c181/img/TiK_IlmoBot%4025.png)

This project is designated to automate enrolling to Tietokilta's events. It is particularry useful in cases where the enrollings fill up very quickly, and thus automating the enrolling will greatly improve your chances to get enrolled to the event before it fills up.

## Technologies

This project uses technologies such as [Robot Framework](http://robotframework.org/) and through it [Selenium](https://www.seleniumhq.org/), additionally, it uses shellscripting [GNU bash](https://www.gnu.org/software/bash/) to poll and actually execute Robot Framework.

## How to use this

TBD

## To-Do

* Implement Robot "test" to enroll to events
* UX

## Author

Axel Latvala, a member of Tietokilta since 2015
